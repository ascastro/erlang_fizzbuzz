-module(fizzbuzz).

-export([run/1]).

run(N) ->
  run(1, N).

run(N, Limit) when N > Limit ->
  ok;
run(N, Limit) ->
  fizzbuzz(N),
  run(N + 1, Limit).

fizzbuzz(N) when N rem 3 =:= 0, N rem 5 =:= 0 ->
  io:format("FizzBuzz\n");
fizzbuzz(N) when N rem 3 =:= 0 ->
  io:format("Fizz\n");
fizzbuzz(N) when N rem 5 =:= 0 ->
  io:format("Buzz\n");
fizzbuzz(N) ->
  io:format("~p\n", [N]).
