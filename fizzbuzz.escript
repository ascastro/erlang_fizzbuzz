#! /usr/bin/env escript

%% -*- erlang -*-
%%! -smp enable -sname fizzbuzz -mnesia debug verbose
main([Limit]) ->
  try
    L = list_to_integer(Limit),
    [fizzbuzz(E) || E <- lists:seq(1, L)],
    ok
  catch
    _:_ ->
        usage()
  end;
main(_) ->
    usage().

usage() ->
    io:format("usage: fizzbuzz limit\n"),
    halt(1).

fizzbuzz(N) when N rem 3 =:= 0, N rem 5 =:= 0 ->
  io:format("FizzBuzz\n");
fizzbuzz(N) when N rem 3 =:= 0 ->
  io:format("Fizz\n");
fizzbuzz(N) when N rem 5 =:= 0 ->
  io:format("Buzz\n");
fizzbuzz(N) ->
  io:format("~p\n", [N]).
